#include <stdio.h>
#include "estudiantes.h"
#include <stdlib.h>
#include <math.h>

int alum = 24; //catidad de alumnos

float desvStd(float media, float prom[]){//funcion que calcula la desviacion standart
	float variacion = 0.0;//asignacion variables, i como contador y variacion lo describe su nombre
	int i = 0;//contador
	
	while(i<alum){//se repite 24 veces(mientras el contador sea menor al numero de alumnos)
		variacion = variacion + (pow((prom[i] - media) , 2));//se calcula la variacion
		i++;//se suma al contador
	}
	variacion = variacion/((float)alum-1);
	variacion = sqrtf(variacion);//ultimos pasos para tener la variacion
	return (variacion);//retorna la variacion a la variable que llamo esta funcion
}
float menor(int i, float nmin, float prom[]){
	if (prom[i] < nmin){//se recorre los promedios y almacena el menor
			return prom[i];
		}
		else{
			return nmin;//devuelve el promedio menor a la variable que llamo esta funcion
		}
	}
float mayor(int i, float nmax, float prom[]){
	if (prom[i] > nmax){//se recorre los promedios y almacena el mayor
		return prom[i];
	}
	else{
		return nmax;//devuelve el promedio mayor a la variable que llamo esta funcion
	}
}


void registroCurso(estudiante curso[]){
	float promalum;//variable para calcular el promedio de los alumnos
	int i;//contador
	for (i=0;i<alum;i++){//se asigna el valor a i, se repite 24 veces( la cantidad de alumnos) y avaza de a 1
		printf("%s %s %s\n", curso[i].nombre,curso[i].apellidoP, curso[i].apellidoM);
		printf("notas de los 3 proyectos: \n");// se busca al alumno y le pregunta al usuario por la nota de sus proyectos
		scanf("%f %f %f", &curso[i].asig_1.proy1, &curso[i].asig_1.proy2, &curso[i].asig_1.proy3);
		printf("notas de los 6 controles: \n");// le pregunta al alumnos por las 6 notas del control
		scanf("%f %f %f %f %f %f", &curso[i].asig_1.cont1, &curso[i].asig_1.cont2, &curso[i].asig_1.cont3, &curso[i].asig_1.cont4, &curso[i].asig_1.cont5, &curso[i].asig_1.cont6);
		promalum = (curso[i].asig_1.proy1)*0.2 + (curso[i].asig_1.proy2)*0.2 + (curso[i].asig_1.proy3)*0.3 + (curso[i].asig_1.cont1)*0.05 + (curso[i].asig_1.cont2)*0.05 + (curso[i].asig_1.cont3)*0.05 + (curso[i].asig_1.cont4)*0.05 + (curso[i].asig_1.cont5)*0.05 + (curso[i].asig_1.cont6)*0.05;
		curso[i].prom = promalum;//arriba se calcula el promedio de acuerdo a nuestro ramo y aca se guarda en la variable dentro del archivo
	}
}

void clasificarEstudiantes(char path[], estudiante curso[]){
	FILE *reprobados;//se asignan 2 variables que seran archivos
	FILE *aprobados;
	int i;//contador
	aprobados = fopen("estuAprobados.txt", "w");//se abren los 2 documentos que almacenaran los nombres de los alumnos aprobados y desaprovados
	reprobados = fopen("estuReprobados.txt", "w");
	for (i=0;i<24;i++){//lo mismo, se repite 24 veces por la cantidad de alumnos
		if (curso[i].prom<4.0){//si los alumnos tienen nota menor a 4 desaprovaron y van al archivo correspondiente
			fprintf(reprobados, "%s %s %s \n", curso[i].nombre, curso[i].apellidoP, curso[i].apellidoM);
		}//en caso contario los almacena en el archivo de aprobados
		else{
			fprintf(aprobados, "%s %s %s \n", curso[i].nombre, curso[i].apellidoP, curso[i].apellidoM);
		}
	}
	fclose(reprobados);//se cierran los archivos
	fclose(aprobados);
}


void metricasEstudiantes(estudiante curso[]){
		int i = 0;//contador, y variables que ocuparemos
		float prom[alum], media = 0.0, dstd, nmin = 7.0, nmax = 1.0;
		//prom[alum] es un arreglo donde almacenaremos los promedios de los alumnos
		while(i<alum){//		se repite 24 por cantidad de alumnos
			prom[i] = curso[i].prom;//se recorren los promedios guardados para almacenarlos en oytra variable
			nmax = mayor(i, nmax, prom);//el nmax invoca la funcion y guarda el resultado
			nmin = menor(i, nmin, prom);//lo mismo con otra funcion
			media = media + prom[i];//va  sumando en cada repeticion
			i++;//se aumenta el contador para salir del while eventualmente
		}
		media = (media/alum);//el numero que calculamos de media se divide al numero de alumnos
		dstd = desvStd(media, prom);//dstd invoca la funcion y guarda la desviacion standart en si misma
		printf("\n la nota mayor es: %.1f\n la nota menor es: %.1f\n la desviacion estandart es: %.1f\n", nmax, nmin, dstd);
		//muestra por pantalla la informacion
}

void menu(estudiante curso[]){
	int opcion;
    do{
		printf( "\n   1. Cargar Estudiantes");
		printf( "\n   2. Ingresar notas");
		printf( "\n   3. Mostrar Promedios");
		printf( "\n   4. Almacenar en archivo" );
		printf( "\n   5. Clasificar Estudiantes");
		printf( "\n   6. Salir.");
		printf( "\n\n   Introduzca opción (1-6): ");
		scanf( "%d", &opcion);
        switch (opcion){
			case 1: cargarEstudiantes("estudiantes.txt", curso); 
					break;

			case 2:  registroCurso(curso);
					break;

			case 3: metricasEstudiantes(curso); 
					break;

			case 4: grabarEstudiantes("test.txt",curso);
					break;	
					
      case 5: clasificarEstudiantes("destino", curso); 
        	break;
         }

    } while (opcion != 6);
}

int main()
{
	estudiante curso[30]; 
	menu(curso); 
	return EXIT_SUCCESS; 
}